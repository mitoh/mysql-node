var express = require('express'),
    app = express(),
    mysql      = require('mysql'),
    bodyParser = require('body-parser'),
    fs = require("fs"),
    config = JSON.parse(fs.readFileSync("./config/config.json", "utf8"))


var connection = mysql.createConnection({
  host     : config.db.host,
  user     : config.db.user,
  password : config.db.pass,
  database : config.db.name
});

app.use(bodyParser.json({limit: config.app.mbLimit}));
app.use(bodyParser.urlencoded({limit: config.app.mbLimit, extended: config.app.extended}));

connection.connect();
app.mysqlConn = connection;

var crudRoute = require("./routes/crudRoute")
crudRoute(app);

app.listen(config.app.port);
console.log("Aplicacion corriendo en puerto: " + config.app.port)