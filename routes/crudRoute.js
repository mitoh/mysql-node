'use strict'

module.exports = function (app) {

    var testController = require("../controllers/testController")

    /**
     * Middleware
     */
    app.use(function (req, res, next) {
        req.mysqlConn = app.mysqlConn
        next()
    });

    app.route("/usuario")
        .get(testController.allUsers)

}